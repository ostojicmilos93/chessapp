import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private apiService: ApiService) { }

  signIn(email, password) {
    return this.apiService.post('Accounts/Login', { email, password });
  }

  register(email, password) {
    return this.apiService.post('Users', { email, password });
  }
}
