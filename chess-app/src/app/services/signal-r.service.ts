import { PlayerStatus } from './../models/player-status.model';
import { Message } from './../models/message.model';
import { GameEndReason } from './../models/game-end-reason.enum';
import { GameEndInfo } from './../models/game-end-info.model';
import { LocalStorageService } from './local-storage.service';
import { Move } from './../models/move.model';
import { GameInfo } from './../models/game-info.model';
import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';
import * as signalR from '@aspnet/signalr';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {

  private hubConnection: HubConnection;
  public messageSubject = new Subject<Message>();
  public gameStartedSubject = new Subject<GameInfo>();
  public moveMadeSubject = new Subject<Move>();
  public gameEndedSubject = new Subject<GameEndInfo>();
  public drawOfferedSubject = new Subject<string>();
  public challengeReceivedSubject = new Subject<string>();
  public connectionStatusChangedSubject = new Subject<PlayerStatus>();
  public opponentId;

  constructor(private localStorageService: LocalStorageService) { }

  async connect(): Promise<string> {
    this.hubConnection = new HubConnectionBuilder().withUrl(`${environment.wsPath}`, {
      skipNegotiation: true,
      transport: signalR.HttpTransportType.WebSockets,
      accessTokenFactory: () => this.localStorageService.getToken()
    }).build();

    await this.hubConnection.start();

    this.hubConnection.on('gameStarted', this.gameStarted);
    this.hubConnection.on('messageReceived', this.messageReceived);
    this.hubConnection.on('moveMade', this.moveMade);
    this.hubConnection.on('gameEnded', this.gameEnded);
    this.hubConnection.on('drawOffered', this.drawOffered);
    this.hubConnection.on('challengeReceived', this.challengeReceived);
    this.hubConnection.on('connectionStatusChanged', this.connectionStatusChanged);

    return await this.getMyConnectionId();
  }

  async disconnect() {
    await this.hubConnection.stop();
  }

  sendMessage(message: Message) {
    this.hubConnection.invoke('sendMessage', message);

  }

  makeAMove(gameId: string, move: Move) {
    this.hubConnection.invoke('makeAMove', gameId, move);
  }

  startGameOrWaitForOpponent() {
    this.hubConnection.invoke('startGameOrWaitForOpponent');
  }

  gameHasEnded(gameId: string, reason: GameEndReason) {
    this.hubConnection.invoke('gameHasEnded', gameId, reason);
  }

  resign(gameId: string) {
    this.hubConnection.invoke('resign', gameId);
  }

  offerDraw(gameId: string) {
    this.hubConnection.invoke('offerDraw', gameId);
  }

  acceptDraw(gameId: string) {
    this.hubConnection.invoke('acceptDraw', gameId);
  }

  challengePlayer(opponent: string) {
    this.hubConnection.invoke('challengePlayer', opponent);
  }

  acceptChallenge(opponent: string) {
    return this.hubConnection.invoke('acceptChallenge', opponent);
  }

  getOnlinePlayers() {
    return this.hubConnection.invoke('getOnlinePlayers');
  }

  private async getMyConnectionId() {
    return await this.hubConnection.invoke('getConnectionId');
  }

  private gameStarted = (info: GameInfo) => {
    console.log(info);
    this.gameStartedSubject.next(info);
  }

  private messageReceived = message => {
    this.messageSubject.next(message);
  }

  private moveMade = move => this.moveMadeSubject.next(move);

  private gameEnded = gameEndInfo => this.gameEndedSubject.next(gameEndInfo);

  private drawOffered = gameId => this.drawOfferedSubject.next(gameId);

  private challengeReceived = opponent => this.challengeReceivedSubject.next(opponent);

  private connectionStatusChanged = (name, isConnected) =>
    this.connectionStatusChangedSubject.next(new PlayerStatus({ name, isConnected }))
}
