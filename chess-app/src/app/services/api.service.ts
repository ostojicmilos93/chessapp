import { environment } from './../../environments/environment';
import { LocalStorageService } from './local-storage.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient,
              private localStorageService: LocalStorageService) { }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.httpClient.get(`${environment.apiBasePath}/${path}/${params.toString()}`, this.initializeHttpOptions());
  }

  put(path: string, body: any): Observable<any> {
    return this.httpClient.put(`${environment.apiBasePath}/${path}`, body, this.initializeHttpOptions());
  }

  post(path: string, body: any): Observable<any> {
    return this.httpClient.post(`${environment.apiBasePath}/${path}`, body, this.initializeHttpOptions());
  }

  delete(path): Observable<any> {
    return this.httpClient.delete(`${environment.apiBasePath}/${path}`, this.initializeHttpOptions());
  }

  private initializeHttpOptions() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.localStorageService.getToken()
      })
    };
  }
}
