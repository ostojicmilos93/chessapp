import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  getToken() {
    return localStorage.getItem('token');
  }

  setToken(token) {
    localStorage.setItem('token', token);
    const decoded = jwt_decode(token);
    localStorage.setItem('email', decoded.email);
  }

  logOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('email');
  }

  getEmail() {
    return localStorage.getItem('email');
  }
}
