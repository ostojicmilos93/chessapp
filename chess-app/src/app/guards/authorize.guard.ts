import { LocalStorageService } from './../services/local-storage.service';
import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorizeGuard implements CanLoad {

  constructor(private localStorageService: LocalStorageService, private router: Router) {}

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    const jwt = this.localStorageService.getToken();
    if (jwt) return true;

    this.router.navigateByUrl('auth');
    return false;
  }
}
