import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'millisToTime'
})
export class MillisToTimePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    if (!value && value !== 0) return '05:00';

    if (value < 0) return '00:00';

    const totalSeconds = Number(value) / 1000;
    let minutes = Math.floor(totalSeconds / 60).toString();
    if (minutes.length === 1) minutes = `0${minutes}`;
    let seconds = Math.floor(totalSeconds % 60).toString();
    if (seconds.length === 1) seconds = `0${seconds}`;

    return `${minutes}:${seconds}`;
  }

}
