export class PlayerInfo {
  connectionId: string;
  name: string;

  constructor(data) {
    this.connectionId = data.connectionId;
    this.name = data.name;
  }
}
