export enum ChallengeStatus {
  NONE,
  SENT,
  RECEIVED
}
