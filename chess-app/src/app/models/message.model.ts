export class Message {
  sender: string;
  content: string;
  receiverId: string;

  constructor(data) {
    this.sender = data.sender;
    this.content = data.content;
    this.receiverId = data.receiverId;
  }
}
