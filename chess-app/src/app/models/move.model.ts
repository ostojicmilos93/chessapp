export class Move {
  source: string;
  target: string;

  playedAt: Date;

  constructor(data) {
    this.source = data.source;
    this.target = data.target;

    this.playedAt = data.playedAt;
  }
}
