import { GameEndReason } from './game-end-reason.enum';
import { GameResult } from './game-result.enum';
import { GameInfo } from './game-info.model';

export class GameEndInfo {
  gameInfo: GameInfo;
  result: GameResult;
  reason: GameEndReason;

  constructor(data) {
    this.gameInfo = data.gameInfo;
    this.result = data.result;
    this.reason = data.reason;
  }
}
