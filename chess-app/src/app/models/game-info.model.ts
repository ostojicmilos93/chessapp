import { PlayerInfo } from './player-info.model';
import { Move } from './move.model';

export class GameInfo {
  gameId: string;

  white: PlayerInfo;
  black: PlayerInfo;

  whiteTimeLeftInMillis: number;
  blackTimeLeftInMillis: number;

  moves: Move[];

  constructor(data) {
    this.gameId = data.gameId;

    this.white = data.white;
    this.black = data.black;

    this.whiteTimeLeftInMillis = data.whiteTimeLeftInMillis;
    this.blackTimeLeftInMillis = data.blackTimeLeftInMillis;

    this.moves = data.moves;
  }
}
