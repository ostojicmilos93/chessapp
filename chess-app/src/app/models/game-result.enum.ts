export enum GameResult {
  WHITE_HAS_WON,
  DRAW,
  BLACK_HAS_WON
}
