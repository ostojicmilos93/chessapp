import { ChallengeStatus } from './challenge-status.enum';
export class PlayerStatus {
  name: string;
  isConnected: boolean;
  challengeStatus: ChallengeStatus;

  constructor(data) {
    this.name = data.name;
    this.isConnected = data.isConnected;
    this.challengeStatus = data.challengeStatus || ChallengeStatus.NONE;
  }
}
