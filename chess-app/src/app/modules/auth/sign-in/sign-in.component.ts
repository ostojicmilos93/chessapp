import { UserService } from './../../../services/user.service';
import { LocalStorageService } from './../../../services/local-storage.service';
import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  user: any = {};
  showError = false;

  constructor(private router: Router,
              private userService: UserService,
              private localStorageService: LocalStorageService,
              private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }

  @HostListener('document:keydown.enter', ['$event'])
  login() {
    this.spinner.show();

    this.userService.signIn(this.user.email, this.user.password).subscribe(
      token => {
        this.localStorageService.setToken(token);
        this.router.navigateByUrl('play');
      }, () => {
        this.showError = true;
      }
    ).add(() => this.spinner.hide());
  }

}
