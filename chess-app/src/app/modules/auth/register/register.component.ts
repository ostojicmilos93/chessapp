import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit, HostListener } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user: any = {};
  showError = false;

  constructor(private userService: UserService,
              private router: Router,
              private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }

  @HostListener('document:keydown.enter', ['$event'])
  register() {
    this.spinner.show();

    this.userService.register(this.user.email, this.user.password).subscribe(
      () => this.router.navigateByUrl('auth/login'),
      () => this.showError = true
    ).add(() => this.spinner.hide());
  }

}
