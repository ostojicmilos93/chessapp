import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';

import { AuthRoutingModule } from './auth-routing.module';
import { RegisterComponent } from './register/register.component';
import { SignInComponent } from './sign-in/sign-in.component';


const MODULES = [
  SharedModule,
  AuthRoutingModule
];

const COMPONENTS = [
  RegisterComponent,
  SignInComponent
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES]
})
export class AuthModule { }
