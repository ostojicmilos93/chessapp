import { MillisToTimePipe } from './../../pipes/millis-to-time.pipe';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';

import { PlayRoutingModule } from './play-routing.module';
import { ChatComponent } from './chat/chat.component';
import { GameComponent } from './game/game.component';
import { PlayComponent } from './play/play.component';
import { OnlinePlayersComponent } from './online-players/online-players.component';


const MODULES = [
  SharedModule,
  PlayRoutingModule
];

const COMPONENTS = [
  ChatComponent, GameComponent, PlayComponent, OnlinePlayersComponent
];

const PIPES = [
  MillisToTimePipe
];

@NgModule({
  declarations: [...COMPONENTS, ...PIPES],
  imports: [...MODULES]
})
export class PlayModule { }
