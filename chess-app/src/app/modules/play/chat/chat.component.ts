import { Message } from './../../../models/message.model';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  text = '';
  @Output() send = new EventEmitter<string>();
  @Input() messages: Message[];

  constructor() { }

  ngOnInit(): void {
  }

  sendMessage() {
    if (!this.text) return;

    this.send.emit(this.text);

    this.text = '';
  }

}
