import { ChallengeStatus } from './../../../models/challenge-status.enum';
import { PlayerStatus } from './../../../models/player-status.model';
import { Router } from '@angular/router';
import { Message } from './../../../models/message.model';
import { GameEndInfo } from './../../../models/game-end-info.model';
import { Move } from './../../../models/move.model';
import { LocalStorageService } from './../../../services/local-storage.service';
import { GameInfo } from './../../../models/game-info.model';
import { SignalRService } from './../../../services/signal-r.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

import * as Chess from 'chess.js';
import { GameResult } from 'src/app/models/game-result.enum';
import { GameEndReason } from 'src/app/models/game-end-reason.enum';
import { Subscription } from 'rxjs';

declare let ChessBoard: any;

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit, OnDestroy {
  messages: Message[] = [];
  onlinePlayers: PlayerStatus[] = [];
  board: any;
  game: any;
  gameInfo: GameInfo;
  amWhite: boolean;
  myName: string;
  opponentsName: string;
  connectionId: string;
  isWhiteToMove: boolean;
  drawHasBeenOffered: boolean;
  isGameInProgress: boolean;
  rematchHasBeenOffered: boolean;
  myTime = 300000;
  opponentsTime = 300000;
  myResult;
  opponentsResult;
  timer;
  subscriptions: Subscription[] = [];

  constructor(private signalRService: SignalRService,
              private localStorageService: LocalStorageService,
              private router: Router) {
    this.myName = this.localStorageService.getEmail();
  }

  async ngOnInit() {
    this.messages.push(new Message({ sender: 'SYSTEM', content: 'You can chat with your opponent once the game starts.' }));

    this.setupBoard({});

    this.connectionId = await this.signalRService.connect();

    this.subscriptions.push(this.signalRService.messageSubject.subscribe(message => this.messages.push(message)));
    this.subscriptions.push(this.signalRService.gameStartedSubject.subscribe(gameInfo => this.gameStarted(gameInfo)));
    this.subscriptions.push(this.signalRService.moveMadeSubject.subscribe(move => this.moveMade(move)));
    this.subscriptions.push(this.signalRService.gameEndedSubject.subscribe(gameEndInfo => this.gameEnded(gameEndInfo)));
    this.subscriptions.push(this.signalRService.drawOfferedSubject.subscribe(gameId =>  {
      this.drawHasBeenOffered = true;
      this.messages.push(new Message({ sender: 'SYSTEM', content: 'Your opponent has made a draw offer.' }));
    }));
    this.subscriptions.push(this.signalRService.challengeReceivedSubject.subscribe(opponent => this.challengeReceived(opponent)));
    this.subscriptions.push(this.signalRService.connectionStatusChangedSubject
      .subscribe(status => this.connectionStatusChanged(status)));

    this.signalRService.getOnlinePlayers().then(players => {
      for (const player of players.filter(p => p !== this.myName))
        this.onlinePlayers.push(new PlayerStatus({ name: player, isConnected: true }));
    });
  }

  setupBoard(configuration) {
    const defaultConfig = {
      draggable: true,
      position: 'start',
      onDragStart: this.onDragStart,
      onDrop: this.onDrop,
      onSnapEnd: this.onSnapEnd
    };
    this.board = ChessBoard('game-board', {...defaultConfig, ...configuration });

    this.game = new Chess();
  }

  ngOnDestroy(): void {
    if (this.gameInfo?.gameId) this.signalRService.resign(this.gameInfo.gameId);

    this.signalRService.disconnect();

    for (const subscription of this.subscriptions) subscription.unsubscribe();

    if (this.timer) clearInterval(this.timer);
  }

  connectionStatusChanged(status: PlayerStatus) {
    if (status.isConnected) this.onlinePlayers.splice(0, 0, status);
    else this.onlinePlayers = this.onlinePlayers.filter(e => e.name !== status.name);
  }

  onDragStart = (source, piece, position, orientation) => {
    if (this.isGameOver() || this.isDraggingWrongColoredPiece(piece)) return false;
  }

  isGameOver = () => this.game.game_over();

  isDraggingWrongColoredPiece = piece => {
    return (this.amWhite && piece.search(/^b/) !== -1) ||
           (!this.amWhite && piece.search(/^w/) !== -1);
  }

  onDrop = (source, target) => {
    if (!this.isMoveLegal(source, target)) return 'snapback';

    this.isWhiteToMove = !this.isWhiteToMove;
    this.signalRService.makeAMove(this.gameInfo.gameId, new Move({ source, target }));
  }

  isMoveLegal = (source, target) => {
    const move = this.game.move({
      from: source,
      to: target,
      promotion: 'q'
    });

    return move !== null;
  }

  moveMade(move: Move) {
    const gameMove = this.game.move({
      from: move.source,
      to: move.target,
      promotion: 'q'
    });

    this.isWhiteToMove = !this.isWhiteToMove;
    this.game.move(gameMove);
    this.board.position(this.game.fen());

    if (this.game.in_checkmate()) {
      this.signalRService.gameHasEnded(this.gameInfo.gameId, GameEndReason.CHECKMATE);
    } else if (this.game.in_stalemate()) {
      this.signalRService.gameHasEnded(this.gameInfo.gameId, GameEndReason.STALEMATE);
    } else if (this.game.insufficient_material()) {
      this.signalRService.gameHasEnded(this.gameInfo.gameId, GameEndReason.INSUFFICIENT_MATERIAL);
    }
  }

  onSnapEnd = () => {
    this.board.position(this.game.fen())
  }

  startGame() {
    this.messages.push(new Message({ sender: 'SYSTEM', content: 'A challenge has been sent, waiting for an opponent...'}));
    this.signalRService.startGameOrWaitForOpponent();
  }

  gameStarted(gameInfo: GameInfo) {
    if (this.opponentsName !== gameInfo.black.name && this.opponentsName !== gameInfo.white.name)
      this.messages = [];

    this.amWhite = gameInfo.white.connectionId === this.connectionId;
    this.opponentsName = this.amWhite ? gameInfo.black.name : gameInfo.white.name;
    this.myTime = this.amWhite ? gameInfo.whiteTimeLeftInMillis : gameInfo.blackTimeLeftInMillis;
    this.opponentsTime = this.amWhite ? gameInfo.blackTimeLeftInMillis : gameInfo.whiteTimeLeftInMillis;
    this.myResult = '';
    this.opponentsResult = '';
    this.setupBoard({ orientation: this.amWhite ? 'white' : 'black' });
    this.isWhiteToMove = true;
    this.isGameInProgress = true;
    this.rematchHasBeenOffered = false;

    for (const player of this.onlinePlayers) {
      if (player.name === gameInfo.black.name || player.name === gameInfo.white.name) {
        player.challengeStatus = ChallengeStatus.NONE;
      }
    }

    this.gameInfo = gameInfo;

    this.messages.push(new Message({ sender: 'SYSTEM', content: 'A new game has started!' }));
    this.messages.push(new Message({ sender: 'SYSTEM', content: 'You can start chatting with your opponent.' }));

    this.timer = setInterval(this.updateTime, 1000);
  }

  resign() {
    this.signalRService.resign(this.gameInfo.gameId);
  }

  offerDraw() {
    this.messages.push(new Message({ sender: 'SYSTEM', content: 'You have offered draw to your opponent.' }));

    this.signalRService.offerDraw(this.gameInfo.gameId);
  }

  acceptDraw() {
    this.signalRService.acceptDraw(this.gameInfo.gameId);
  }

  offerRematch() {
    if (!this.gameInfo) return;

    this.messages.push(new Message({ sender: 'SYSTEM', content: 'You have offered a rematch to your opponent.' }));

    this.signalRService.challengePlayer(this.opponentsName);
  }

  acceptRematch() {

    this.signalRService.acceptChallenge(this.opponentsName).then(success => {
      const content = success ? 'You have accepted the rematch offer.'
                              : `${this.opponentsName} is already playing another game.`;

      this.messages.push(new Message({ sender: 'SYSTEM', content  }));
    });
  }

  acceptChallenge(player: PlayerStatus) {
    player.challengeStatus = ChallengeStatus.NONE;

    this.signalRService.acceptChallenge(player.name).then(success => {
      const content = success ? 'You have accepted the challenge.'
                              : `${player.name} is already playing another game.`;

      this.messages.push(new Message({ sender: 'SYSTEM', content }));
    });
  }

  updateTime = () => {
    if (this.IShouldMove) this.myTime -= 1000;
    else this.opponentsTime -= 1000;

    if (this.myTime <= -1000 || this.opponentsTime <= -1000) {
      this.signalRService.gameHasEnded(this.gameInfo.gameId, GameEndReason.TIME_RAN_OUT);
    }
  }

  get IShouldMove() {
    return (this.isWhiteToMove && this.amWhite) || (!this.isWhiteToMove && !this.amWhite);
  }

  gameEnded(gameEndInfo: GameEndInfo) {
    if (this.gameInfo.gameId !== gameEndInfo.gameInfo.gameId) return;

    clearInterval(this.timer);

    this.displayGameResult(gameEndInfo);

    this.drawHasBeenOffered = false;
    this.isGameInProgress = false;
  }

  displayGameResult(gameEndInfo: GameEndInfo) {
    const IHaveWon = (gameEndInfo.result === GameResult.WHITE_HAS_WON && this.amWhite) ||
                     (gameEndInfo.result === GameResult.BLACK_HAS_WON && !this.amWhite);
    if (gameEndInfo.result === GameResult.DRAW) {
      this.myResult = '0.5';
      this.opponentsResult = '0.5';
    } else if (IHaveWon) {
      this.myResult = '1.0';
      this.opponentsResult = '0.0';
    } else {
      this.myResult = '0.0';
      this.opponentsResult = '1.0';
    }

    let message = 'The game has ended peacefully';
    if (gameEndInfo.result === GameResult.WHITE_HAS_WON) message = 'White has won the game!';
    else if (gameEndInfo.result === GameResult.BLACK_HAS_WON) message = 'Black has won the game!';

    this.messages.push(new Message({ sender: 'SYSTEM', content: message }));
  }

  challengeReceived(opponent) {
    if (opponent === this.opponentsName) {
      this.rematchHasBeenOffered = true;

      this.messages.push(new Message({ sender: 'SYSTEM', content: 'Your opponent has offered a rematch.' }));
    } else {
      for (const player of this.onlinePlayers) {
        if (player.name === opponent) {
          player.challengeStatus = ChallengeStatus.RECEIVED;
          this.messages.push(new Message({ sender: 'SYSTEM', content: `${opponent} has challenged you to a game.` }));
          break;
        }
      }
    }
  }

  challengePlayer(player: PlayerStatus) {
    player.challengeStatus = ChallengeStatus.SENT;

    this.signalRService.challengePlayer(player.name);
  }

  sendMessage(content) {
    if (!this.gameInfo) {
      this.messages.push(new Message({ sender: 'SYSTEM', content: 'You can only chat while a game is in progress.' }));
      return;
    }

    const receiverId = this.amWhite ? this.gameInfo.black.connectionId : this.gameInfo.white.connectionId;

    const message = new Message({ sender: this.myName, content, receiverId });
    this.messages.push(message);
    this.signalRService.sendMessage(message);
  }

  logOut() {
    this.localStorageService.logOut();
    this.router.navigateByUrl('auth');
  }
}
