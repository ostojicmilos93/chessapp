import { PlayerStatus } from './../../../models/player-status.model';
import { ChallengeStatus } from './../../../models/challenge-status.enum';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-online-players',
  templateUrl: './online-players.component.html',
  styleUrls: ['./online-players.component.css']
})
export class OnlinePlayersComponent implements OnInit {
  @Input() players: string[];

  @Output() challenge = new EventEmitter<PlayerStatus>();
  @Output() accept = new EventEmitter<PlayerStatus>();

  statuses = ChallengeStatus;

  constructor() { }

  ngOnInit(): void {
  }

  challengePlayer(player: PlayerStatus) {
    this.challenge.emit(player);
  }

  acceptChallenge(player: PlayerStatus) {
    this.accept.emit(player);
  }

}
