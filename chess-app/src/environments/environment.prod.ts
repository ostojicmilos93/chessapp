export const environment = {
  production: true,
  apiBasePath: 'http://173.249.5.45:8686/api',
  wsPath: 'http://173.249.5.45:8686/chess-hub'
};
