﻿using ChessBackend.Helpers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChessBackend.Services.Business
{
    public abstract class DbService<TModel, TDto, TContext> : IService<TDto>
        where TModel : class
        where TContext : DbContext
    {
        protected TContext Context { get; }
        protected DbSet<TModel> ModelRepository { get; }

        protected DbService(TContext context)
        {
            Context = context;
            ModelRepository = Context.Set<TModel>();
        }

        public async Task<IEnumerable<TDto>> GetAllAsync()
        {
            var models = await PopulateQuery(ModelRepository).ToListAsync();

            return models.Select(Mapster.TypeAdapter.Adapt<TModel, TDto>);
        }

        public async Task<TDto> FindByIdAsync(params object[] key)
        {
            var model = await ModelRepository.FindAsync(key);
            await PopulateModelAsync(model);

            return Mapster.TypeAdapter.Adapt<TModel, TDto>(model);
        }

        public async Task<TDto> AddAsync(TDto dto)
        {
            var model = Mapster.TypeAdapter.Adapt<TDto, TModel>(dto);
            await AfterModelMappingAsync(dto, model);

            await Context.AddAsync(model);

            await Context.SaveChangesAsync();

            return Mapster.TypeAdapter.Adapt<TModel, TDto>(model);
        }

        public async Task UpdateAsync(TDto dto)
        {
            var model = Mapster.TypeAdapter.Adapt<TDto, TModel>(dto);
            await AfterModelMappingAsync(dto, model);

            Context.Update(model);
        }

        public async Task AddOrUpdateAsync(TDto dto)
        {
            var model = Mapster.TypeAdapter.Adapt<TDto, TModel>(dto);

            await Context.Entry(model).ReloadAsync(); // Use reload to check if model is present in db

            if (Context.Entry(model).State == EntityState.Detached) // EntityState.Detached == not in db, see ReloadAsync() docs
                await AddAsync(dto);
            else
            {
                Context.Entry(model).State = EntityState.Detached; // Discard the model created above
                await UpdateAsync(dto);
            }
        }

        public async Task<TDto> RemoveAsync(object key)
        {
            var model = await ModelRepository.FindAsync(key);

            Context.Remove(model);

            return Mapster.TypeAdapter.Adapt<TModel, TDto>(model);
        }

        // Util

        private async Task PopulateModelAsync(TModel model)
        {
            await Context.Entry(model).Navigations.ForEachAsync(async x => await x.LoadAsync());
        }

        protected virtual IQueryable<TModel> PopulateQuery(DbSet<TModel> modelRepository)
        {
            return modelRepository;
        }

        protected virtual async Task AfterModelMappingAsync(TDto dto, TModel model)
        {
            await Task.CompletedTask;
        }
    }
}
