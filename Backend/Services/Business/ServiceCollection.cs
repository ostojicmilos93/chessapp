﻿using ChessBackend.Repository;
using System.Threading.Tasks;

namespace ChessBackend.Services.Business
{
    public class ServiceCollection
    {
        private IDbContextTransaction Transaction { get; }
        public Users Users { get; set; }

        public ServiceCollection(IDbContextTransaction transaction,
                                 Users users)
        {
            Transaction = transaction;
            Users = users;
        }

        public async Task<int> SaveChangesAsync()
        {
            return await Transaction.CommitAsync();
        }
    }
}
