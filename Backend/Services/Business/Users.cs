﻿using ChessBackend.Data.Models;
using ChessBackend.Data.Roles;
using ChessBackend.Repository;
using ChessBackend.Repository.Models;
using ChessBackend.Services.Mapping;
using Mapster;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace ChessBackend.Services.Business
{
    public class Users : DbService<User, UserDto, ApplicationContext>
    {
        public UserManager<User> UserManager { get; set; }

        static Users() => MapperConfig.RegisterUserMapping();

        public Users(ApplicationContext context, UserManager<User> userManager) : base(context)
        {
            UserManager = userManager;
        }

        public async Task<bool> CreateUserAsync(UserDto dto, string password)
        {
            var user = TypeAdapter.Adapt<User>(dto);
            user.UserName = dto.Email;

            var result = await UserManager.CreateAsync(user, password);
            if (!result.Succeeded) return false;

            await UserManager.AddToRoleAsync(user, RoleConstants.PLAYER);

            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var user = await UserManager.FindByIdAsync(id.ToString());

            var result = await UserManager.DeleteAsync(user);

            return result.Succeeded;
        }
    }
}
