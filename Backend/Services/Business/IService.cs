﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChessBackend.Services.Business
{
    public interface IService<TDto>
    {
        Task<IEnumerable<TDto>> GetAllAsync();
        Task<TDto> FindByIdAsync(params object[] key);
        Task<TDto> AddAsync(TDto dto);
        Task UpdateAsync(TDto dto);
        Task<TDto> RemoveAsync(object key);
    }
}
