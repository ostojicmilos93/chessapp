﻿using ChessBackend.Data.Models;
using ChessBackend.Repository.Models;
using Mapster;

namespace ChessBackend.Services.Mapping
{
    public static class MapperConfig
    {
        public static void RegisterUserMapping()
        {
            TypeAdapterConfig<User, UserDto>.NewConfig();
            TypeAdapterConfig<UserDto, User>.NewConfig();
        }
    }
}
