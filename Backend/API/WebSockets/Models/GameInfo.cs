﻿using ChessBackend.API.WebSockets.Models;
using System;
using System.Collections.Generic;

namespace ChessBackend.API.WebSockets
{
    public class GameInfo
    {
        public Guid GameId { get; set; }

        public PlayerInfo White { get; set; }
        public PlayerInfo Black { get; set; }

        public long WhiteTimeLeftInMillis { get; set; }
        public long BlackTimeLeftInMillis { get; set; }

        public DateTime StartedAt { get; set; }
        public List<Move> Moves { get; set; } = new List<Move>();
    }
}
