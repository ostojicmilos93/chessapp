﻿namespace ChessBackend.API.WebSockets.Models
{
    public class GameEndInfo
    {
        public GameInfo GameInfo { get; set; }
        public GameResult Result { get; set; }
        public GameEndReason Reason { get; set; }
    }
}
