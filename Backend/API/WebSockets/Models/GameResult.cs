﻿namespace ChessBackend.API.WebSockets.Models
{
    public enum GameResult
    {
        WHITE_HAS_WON,
        DRAW,
        BLACK_HAS_WON
    }
}
