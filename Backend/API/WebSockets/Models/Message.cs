﻿namespace ChessBackend.API.WebSockets.Models
{
    public class Message
    {
        public string Sender { get; set; }
        public string Content { get; set; }
        public string ReceiverId { get; set; }
    }
}
