﻿namespace ChessBackend.API.WebSockets
{
    public class PlayerInfo
    {
        public string ConnectionId { get; set; }
        public string Name { get; set; }
        public bool IsCurrentlyPlaying { get; set; }
    }
}
