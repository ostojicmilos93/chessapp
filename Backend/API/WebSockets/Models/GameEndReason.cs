﻿namespace ChessBackend.API.WebSockets.Models
{
    public enum GameEndReason
    {
        CHECKMATE,
        STALEMATE,
        INSUFFICIENT_MATERIAL,
        TIME_RAN_OUT,
        AGREEMENT,
        RESIGNATION
    }
}
