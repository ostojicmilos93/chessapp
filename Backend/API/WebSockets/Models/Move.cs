﻿using System;

namespace ChessBackend.API.WebSockets.Models
{
    public class Move
    {
        public string Source { get; set; }
        public string Target { get; set; }

        public DateTime PlayedAt { get; set; } = DateTime.Now;
    }
}
