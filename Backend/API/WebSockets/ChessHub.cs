﻿using ChessBackend.API.WebSockets.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChessBackend.API.WebSockets
{
    [Authorize]
    public class ChessHub : Hub
    {
        internal static ConcurrentDictionary<string, PlayerInfo> OnlinePlayers = new ConcurrentDictionary<string, PlayerInfo>();
        internal static ConcurrentDictionary<string, PlayerInfo> PlayersInLobby = new ConcurrentDictionary<string, PlayerInfo>();
        internal static ConcurrentDictionary<Guid, GameInfo> ActiveGames = new ConcurrentDictionary<Guid, GameInfo>();

        public async override Task OnConnectedAsync()
        {
            var currentUser = GetCurrentUserEmail();
            var playerInfo = new PlayerInfo { ConnectionId = Context.ConnectionId, Name = currentUser };
            OnlinePlayers.AddOrUpdate(currentUser, playerInfo, (key, oldValue) => playerInfo);

            await NotifyPlayersOfConnectionStatusChange(currentUser, true);

            await base.OnConnectedAsync();
        }

        public async override Task OnDisconnectedAsync(Exception exception)
        {
            PlayersInLobby.TryRemove(Context.ConnectionId, out var _);
            var currentUser = GetCurrentUserEmail();
            OnlinePlayers.TryRemove(currentUser, out var _);

            await NotifyPlayersOfConnectionStatusChange(currentUser, false);

            await base.OnDisconnectedAsync(exception);
        }

        public string GetConnectionId()
        {
            return Context.ConnectionId;
        }

        public async Task SendMessage(Message message)
        {
            await Clients.Client(message.ReceiverId).SendAsync("messageReceived", message);
        }

        public async Task GameHasEnded(Guid gameId, GameEndReason reason)
        {
            var isGameActive = ActiveGames.TryRemove(gameId, out var gameInfo);
            if (!isGameActive) return;

            var result = GameResult.DRAW;
            if (reason == GameEndReason.CHECKMATE || reason == GameEndReason.TIME_RAN_OUT)
            {
                result = gameInfo.Moves.Count % 2 == 1 ? GameResult.WHITE_HAS_WON : GameResult.BLACK_HAS_WON;
            }

            SetPlayersInactive(gameInfo);

            await NotifyPlayersThatGameHasEnded(new GameEndInfo
            {
                GameInfo = gameInfo,
                Reason = reason,
                Result = result
            });
        }

        public async Task Resign(Guid gameId)
        {
            var isGameActive = ActiveGames.TryRemove(gameId, out var gameInfo);
            if (!isGameActive) return;

            SetPlayersInactive(gameInfo);

            await NotifyPlayersThatGameHasEnded(new GameEndInfo
            {
                GameInfo = gameInfo,
                Reason = GameEndReason.RESIGNATION,
                Result = Context.ConnectionId.Equals(gameInfo.White.ConnectionId) ? GameResult.BLACK_HAS_WON : GameResult.WHITE_HAS_WON
            });
        }

        public async Task AcceptDraw(Guid gameId)
        {
            var isGameActive = ActiveGames.TryRemove(gameId, out var gameInfo);
            if (!isGameActive) return;

            SetPlayersInactive(gameInfo);

            await NotifyPlayersThatGameHasEnded(new GameEndInfo
            {
                GameInfo = gameInfo,
                Reason = GameEndReason.AGREEMENT,
                Result = GameResult.DRAW
            });
        }

        public async Task OfferDraw(Guid gameId)
        {
            var isGameActive = ActiveGames.TryRemove(gameId, out var gameInfo);
            if (!isGameActive) return;

            ActiveGames.TryAdd(gameId, gameInfo);

            var opponentId = gameInfo.White.ConnectionId.Equals(Context.ConnectionId) ? gameInfo.Black.ConnectionId : gameInfo.White.ConnectionId;

            await Clients.Client(opponentId).SendAsync("drawOffered", gameId);
        }

        public async Task MakeAMove(Guid gameId, Move move)
        {
            var isGameActive = ActiveGames.TryGetValue(gameId, out var gameInfo);
            if (!isGameActive) return;

            var gameEndInfo = UpdateGameInfo(gameInfo, move);
            if (gameEndInfo != null)
            {
                SetPlayersInactive(gameInfo);

                await NotifyPlayersThatGameHasEnded(gameEndInfo);
                return;
            }

            var opponentId = gameInfo.Moves.Count % 2 == 1 ? gameInfo.Black.ConnectionId : gameInfo.White.ConnectionId;
            await Clients.Client(opponentId).SendAsync("moveMade", move);
        }

        private GameEndInfo UpdateGameInfo(GameInfo info, Move move)
        {
            UpdateTimeLeft(info, move.PlayedAt);

            info.Moves.Add(move);

            var whitesTimeRanOut = info.WhiteTimeLeftInMillis <= 0;
            var blacksTimeRanOut = info.BlackTimeLeftInMillis <= 0;
            if (whitesTimeRanOut || blacksTimeRanOut)
            {
                return new GameEndInfo
                {
                    GameInfo = info,
                    Reason = GameEndReason.TIME_RAN_OUT,
                    Result = whitesTimeRanOut ? GameResult.BLACK_HAS_WON : GameResult.WHITE_HAS_WON
                };
            }

            return null;
        }

        private void UpdateTimeLeft(GameInfo gameInfo, DateTime newMovePlayedAt)
        {
            var lastMoveTime = gameInfo.Moves.Any() ? gameInfo.Moves.Last().PlayedAt : gameInfo.StartedAt;
            if (gameInfo.Moves.Count % 2 == 1)
                gameInfo.BlackTimeLeftInMillis -= Convert.ToInt64((newMovePlayedAt - lastMoveTime).TotalMilliseconds);
            else
                gameInfo.WhiteTimeLeftInMillis -= Convert.ToInt64((newMovePlayedAt - lastMoveTime).TotalMilliseconds);
        }

        public async Task ChallengePlayer(string opponent)
        {
            var isOnline = OnlinePlayers.TryGetValue(opponent, out var info);
            if (!isOnline) return;

            await Clients.Client(info.ConnectionId).SendAsync("challengeReceived", GetCurrentUserEmail());
        }

        public async Task<bool> AcceptChallenge(string opponent)
        {
            var currentUser = GetCurrentUserEmail();
            PlayersInLobby.TryRemove(opponent, out var _);
            PlayersInLobby.TryRemove(currentUser, out var _);

            var isOpponentOnline = OnlinePlayers.TryGetValue(opponent, out var opponentInfo);
            OnlinePlayers.TryGetValue(currentUser, out var currentUserInfo);

            if (!isOpponentOnline || opponentInfo.IsCurrentlyPlaying) return false;

            var gameInfo = StartGame(opponentInfo, currentUserInfo);
            await NotifyPlayersThatGameHasStarted(gameInfo);

            return true;
        }

        public async Task StartGameOrWaitForOpponent()
        {
            if (PlayersInLobby.ContainsKey(Context.ConnectionId)) return;

            var currentUser = GetCurrentUserEmail();

            var isOpponentFound = TryToFindAnOpponent(out var opponent);                
            if (!isOpponentFound || opponent.Name.Equals(currentUser))
            {
                AddPlayerToLoby(Context.ConnectionId, currentUser);
                return;
            }

            OnlinePlayers.TryGetValue(currentUser, out var currentUserInfo);
            var gameInfo = StartGame(currentUserInfo, opponent);
            await NotifyPlayersThatGameHasStarted(gameInfo);
        }

        private bool TryToFindAnOpponent(out PlayerInfo opponent)
        {
            opponent = null;

            while (!PlayersInLobby.IsEmpty)
            {
                var firstAvailablePlayer = PlayersInLobby.Keys.First();
                var isOpponentSelected = PlayersInLobby.TryRemove(firstAvailablePlayer, out opponent);
                if (isOpponentSelected) return true;
            }

            return false;
        }

        private void AddPlayerToLoby(string id, string name)
        {
            var isPlayerOnline = OnlinePlayers.TryGetValue(name, out var info);

            if (isPlayerOnline) PlayersInLobby.TryAdd(id, info);
        } 

        private GameInfo StartGame(PlayerInfo playerOne, PlayerInfo playerTwo)
        {
            var isPlayerOneWhite = new Random().NextDouble() > 0.5;

            var gameInfo = new GameInfo
            {
                GameId = Guid.NewGuid(),
                White = isPlayerOneWhite ? playerOne : playerTwo,
                Black = isPlayerOneWhite ? playerTwo : playerOne,
                StartedAt = DateTime.Now,
                WhiteTimeLeftInMillis = 300000,
                BlackTimeLeftInMillis = 300000
            };

            playerOne.IsCurrentlyPlaying = true;
            playerTwo.IsCurrentlyPlaying = true;

            ActiveGames.TryAdd(gameInfo.GameId, gameInfo);

            return gameInfo;
        }

        private async Task NotifyPlayersThatGameHasStarted(GameInfo gameInfo)
        {
            await Clients.Client(gameInfo.White.ConnectionId).SendAsync("gameStarted", gameInfo);
            await Clients.Client(gameInfo.Black.ConnectionId).SendAsync("gameStarted", gameInfo);
        }

        private async Task NotifyPlayersThatGameHasEnded(GameEndInfo gameEndInfo)
        {
            await Clients.Client(gameEndInfo.GameInfo.White.ConnectionId).SendAsync("gameEnded", gameEndInfo);
            await Clients.Client(gameEndInfo.GameInfo.Black.ConnectionId).SendAsync("gameEnded", gameEndInfo);
        }

        private async Task NotifyPlayersOfConnectionStatusChange(string email, bool isConnected)
        {
            await Clients.AllExcept(Context.ConnectionId).SendAsync("connectionStatusChanged", email, isConnected);
        }

        public ICollection<string> GetOnlinePlayers()
        {
            return OnlinePlayers.Keys;
        }

        private string GetCurrentUserEmail()
        {
            return Context.User.Claims.Single(c => c.Type.Contains("email")).Value;
        }

        private void SetPlayersInactive(GameInfo gameInfo)
        {
            var isWhiteOnline = OnlinePlayers.TryGetValue(gameInfo.White.Name, out var white);
            if (isWhiteOnline) white.IsCurrentlyPlaying = false;

            var isBlackOnline = OnlinePlayers.TryGetValue(gameInfo.Black.Name, out var black);
            if (isBlackOnline) black.IsCurrentlyPlaying = false;
        }
    }
}
