﻿using ChessBackend.API.AppConstants;
using ChessBackend.Data.Helpers;
using ChessBackend.Data.Models;
using ChessBackend.Repository.Models;
using ChessBackend.Services.Business;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/Accounts")]
    [Produces("application/json")]
    public class AccountsController : ApiController<Users, UserDto>
    {
        private IConfiguration Configuration { get; }
        private SignInManager<User> SignInManager { get; }
        private UserManager<User> UserManager { get; }

        public AccountsController(ServiceCollection services, Users users, SignInManager<User> signInManager, UserManager<User> userManager, IConfiguration configuration) : base(services, users)
        {
            Configuration = configuration;
            SignInManager = signInManager;
            UserManager = userManager;
        }

        [AllowAnonymous]
        [HttpPost(nameof(Login))]
        public async Task<IActionResult> Login([FromBody] LoginCredentialsDto credentials)
        {
            return await LoginUser(credentials);
        }

        private async Task<IActionResult> LoginUser(LoginCredentialsDto credentials)
        {
            var user = await UserManager.FindByEmailAsync(credentials.Email);
            if (user == null) return BadRequest();

            var result = await SignInManager.CheckPasswordSignInAsync(user, credentials.Password, true);

            if (result.IsLockedOut) return Forbid();

            if (!result.Succeeded) return BadRequest();

            var token = await CreateTokenAsync(user);

            return Ok(token);
        }

        private async Task<string> CreateTokenAsync(User user)
        {
            var claims = await GetClaimsForUserAsync(user);
            var plainKey = Configuration[Constants.JWT_SIGNING_SECRET_KEY];
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(plainKey));
            var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);

            var jwt = new JwtSecurityToken(signingCredentials: signingCredentials, claims: claims);
            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        private async Task<List<Claim>> GetClaimsForUserAsync(User user)
        {
            var roles = await UserManager.GetRolesAsync(user);
            var roleClaims = roles.Select(x => new Claim("role", x));

            var otherClaims = new Claim[]
            {
                new Claim("id", user.Id.ToString()),
                new Claim("email", user.Email)
            };

            var allClaims = otherClaims.Concat(roleClaims).ToList();

            return allClaims;
        }

    }
}