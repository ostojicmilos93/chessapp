﻿using ChessBackend.Data.Helpers;
using ChessBackend.Data.Models;
using ChessBackend.Services.Business;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/Users")]
    [Produces("application/json")]
    public class UsersController : ApiController<Users, UserDto>
    {
        public UsersController(ServiceCollection services, Users users) : base(services, users) { }        

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] RegistrationDto registrationDto)
        {
            var isCreated = await Services.Users.CreateUserAsync(new UserDto { Email = registrationDto.Email }, registrationDto.Password);
            if (!isCreated) return BadRequest();

            return Ok();
        }
    }
}