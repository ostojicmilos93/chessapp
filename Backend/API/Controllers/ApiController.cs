﻿using ChessBackend.Data.Models;
using ChessBackend.Services.Business;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class ApiController<TService, TDto> : Controller
        where TDto : ModelDto<int>
    {
        protected ServiceCollection Services { get; }
        private IService<TDto> Service { get; }

        public ApiController(ServiceCollection services, IService<TDto> service)
        {
            Services = services;
            Service = service;
        }

        protected async Task<IActionResult> GetAllBaseAsync()
        {
            var dtos = await Service.GetAllAsync();

            return Ok(dtos);
        }

        protected async Task<IActionResult> GetSingleBaseAsync([FromRoute] int id)
        {
            var dto = await Service.FindByIdAsync(id);

            return Ok(dto);
        }

        protected async Task<IActionResult> UpdateBaseAsync([FromRoute] int id, [FromBody] TDto dto)
        {
            if (id != dto.Id) return BadRequest();

            await Service.UpdateAsync(dto);

            try
            {
                await Services.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!await ResourceExists(id))
                    return NotFound();
                else
                    throw;
            }

            return NoContent();
        }

        protected async Task<TDto> CreateBaseAsync([FromBody] TDto dto)
        {
            return await Service.AddAsync(dto);
        }

        protected async Task<IActionResult> DeleteBaseAsync([FromRoute] int id)
        {
            var dto = await Service.FindByIdAsync(id);
            if (dto == null) return NotFound();

            await Service.RemoveAsync(id);
            await Services.SaveChangesAsync();

            return Ok(dto);
        }

        // helpers

        protected async Task<bool> ResourceExists(int id)
        {
            var dto = await Service.FindByIdAsync(id);

            return dto != null;
        }

        protected int GetCurrentUserId()
        {
            var idClaim = User.Claims.Single(c => c.Type == "id");

            return Int32.Parse(idClaim.Value);
        }
    }
}