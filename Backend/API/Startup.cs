﻿using ChessBackend.API.AppConstants;
using ChessBackend.API.WebSockets;
using ChessBackend.Repository;
using ChessBackend.Repository.Models;
using ChessBackend.Services.Business;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ChessBackend.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // CORS
            services.AddCors(opt => opt.AddPolicy("cors", builder =>
            {
                builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
            }));

            // EF Context
            services.AddDbContext<ApplicationContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("Default"), b => b.MigrationsAssembly("ChessBackend.API")));

            // Identity
            services.AddSingleton<IUserTwoFactorTokenProvider<User>, DataProtectorTokenProvider<User>>();
            services.AddIdentity<User, IdentityRole<int>>(opt =>
            {
                opt.Password = new PasswordOptions
                {
                    RequireDigit = false,
                    RequiredLength = 5,
                    RequiredUniqueChars = 0,
                    RequireLowercase = false,
                    RequireNonAlphanumeric = false,
                    RequireUppercase = false
                };
                opt.Lockout = new LockoutOptions
                {
                    MaxFailedAccessAttempts = 5,
                    DefaultLockoutTimeSpan = TimeSpan.FromMinutes(15),
                    AllowedForNewUsers = true
                };
                opt.Tokens.ProviderMap.Add("Default", new TokenProviderDescriptor(typeof(IUserTwoFactorTokenProvider<User>)));
            }).AddEntityFrameworkStores<ApplicationContext>();

            // Services
            services.AddScoped<IDbContextTransaction>(sp => sp.GetRequiredService<ApplicationContext>());
            services.AddScoped<Users>();
            services.AddScoped<Services.Business.ServiceCollection>();

            // JWT Auth
            var plainKey = Configuration[Constants.JWT_SIGNING_SECRET_KEY];
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(plainKey));

            services.AddAuthentication(opt =>
            {
                opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(cfg =>
            {
                cfg.RequireHttpsMetadata = false;
                cfg.SaveToken = true;
                cfg.TokenValidationParameters = new TokenValidationParameters
                {
                    IssuerSigningKey = signingKey,
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    ValidateLifetime = false,
                    ValidateIssuerSigningKey = true
                };
                cfg.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        var accessToken = context.Request.Query["access_token"];

                        var path = context.HttpContext.Request.Path;
                        if (!string.IsNullOrEmpty(accessToken) && path.StartsWithSegments("/chess-hub"))
                        {
                            context.Token = accessToken;
                        }
                        return Task.CompletedTask;
                    }
                };
            });

            services.AddSignalR();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Swagger
            services.AddSwaggerGen(c =>
            {
                c.DescribeAllEnumsAsStrings();
                c.SwaggerDoc("v1", new Info { Title = "Core API", Description = "Swagger Core API" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("cors");

            UpdateDatabase(app, env);

            app.UseStaticFiles();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseMvc();

            app.UseSignalR(routes => { routes.MapHub<ChessHub>("/chess-hub"); });

            // Swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Core API");
            });
        }

        private void UpdateDatabase(IApplicationBuilder app, IHostingEnvironment env)
        {
            var scopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
            using (var scope = scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationContext>();

                // Migrate to latest version
                dbContext.Database.Migrate();

                // Seed
                var seedResource = env.IsDevelopment() ? GetResourceFromThisAssembly("Seeds.TestData.sql")
                                                       : GetResourceFromThisAssembly("Seeds.Production.sql");

                var seedSql = new StreamReader(seedResource).ReadToEnd();

                try { dbContext.Database.ExecuteSqlCommand(seedSql); }
                catch (Exception exc) { /* Ignore seed errors */ }
            }
        }

        private Stream GetResourceFromThisAssembly(string resource)
        {
            var assembly = typeof(Startup).Assembly;
            var assemblyName = assembly.GetName().Name;

            return assembly.GetManifestResourceStream($"{assemblyName}.{resource}");
        }
    }
}
