﻿namespace ChessBackend.API.AppConstants
{
    public static class Constants
    {
        public const string JWT_SIGNING_SECRET_KEY = "JWT_SIGNING_SECRET_KEY";
    }
}
