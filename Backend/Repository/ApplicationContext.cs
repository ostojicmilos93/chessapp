﻿using ChessBackend.Repository.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace ChessBackend.Repository
{
    public class ApplicationContext : IdentityDbContext<User, IdentityRole<int>, int>, IDbContextTransaction
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public async Task<int> CommitAsync()
        {
            return await SaveChangesAsync();
        }
    }
}
