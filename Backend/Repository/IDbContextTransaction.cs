﻿using System;
using System.Threading.Tasks;

namespace ChessBackend.Repository
{
    public interface IDbContextTransaction : IDisposable
    {
        Task<int> CommitAsync();
    }
}
