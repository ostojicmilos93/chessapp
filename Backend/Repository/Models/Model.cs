﻿using System;

namespace ChessBackend.Repository.Models
{
    public class Model<TKey>
        where TKey : IComparable, IComparable<TKey>, IEquatable<TKey>
    {
        public TKey Id { get; set; }
    }
}
