﻿using Microsoft.AspNetCore.Identity;

namespace ChessBackend.Repository.Models
{
    public class User : IdentityUser<int>
    {
    }
}
