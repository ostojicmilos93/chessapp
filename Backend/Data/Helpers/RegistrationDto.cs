﻿namespace ChessBackend.Data.Helpers
{
    public class RegistrationDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
