﻿namespace ChessBackend.Data.Helpers
{
    public class LoginCredentialsDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
