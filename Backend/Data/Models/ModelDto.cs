﻿using System;

namespace ChessBackend.Data.Models
{
    public abstract class ModelDto<TKey>
        where TKey : IComparable, IComparable<TKey>, IEquatable<TKey>
    {
        public TKey Id { get; set; }
    }
}
