﻿namespace ChessBackend.Data.Models
{
    public class UserDto : ModelDto<int>
    {
        public string Email { get; set; }
    }
}
